const path = require('path');
const webpack = require('webpack');
// const CriticalPlugin = require('webpack-plugin-critical').CriticalPlugin;

const res = p => path.resolve(__dirname, p);

const entry = res('../server/render.js');
const output = res('../../dist/buildServer');
const autoprefixer = require('autoprefixer');

const cssLoaders = [{
  loader: 'css-loader/locals',
  options: {
    modules: true,
    localIdentName: '[name]__[local]--[hash:base64:5]',
  }
}, {
  loader: 'postcss-loader',
  options: {
    plugins: () => [autoprefixer('last 2 versions')]
  }
}, {
  loader: 'sass-loader',
  options: {
    data: '@import "variables";',
    includePaths: [ res('../static/scss') ]
  }
}];

module.exports = {
  name: 'server',
  target: 'node',
  externals: ['redis'],
  entry: {
    bootstrap: [res('../static/scss/main.scss')],
    main: [entry]
  },
  output: {
    path: output,
    filename: '[name].js',
    libraryTarget: 'commonjs2'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        exclude: [/node_modules/],
        use: cssLoaders
      }
    ]
  },
  resolve: {
    alias: {
      'components': res('../shared/components'),
      'containers': res('../shared/containers'),
      'shared': res('../shared'),
      'static': res('../static'),
      'react': 'preact-compat',
      'react-dom': 'preact-compat'
    }
  },
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
        API_URL: JSON.stringify('http://localhost:3000'),
        browser: false
      }
    })
  ]
}
