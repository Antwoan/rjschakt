const path = require('path');
const webpack = require('webpack');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const ExtractCssChunksVendors = require('extract-css-chunks-webpack-plugin');
const StatsPlugin = require('stats-webpack-plugin');
const autoprefixer = require('autoprefixer');
const WebpackBundleSizeAnalyzerPlugin = require('webpack-bundle-size-analyzer').WebpackBundleSizeAnalyzerPlugin;
const res = p => path.resolve(__dirname, p);

const cssLoaders = [{
  loader: 'css-loader',
  options: {
    modules: true,
    localIdentName: '[name]__[local]--[hash:base64:5]',
  }
}, {
  loader: 'postcss-loader',
  options: {
    plugins: () => [autoprefixer('last 2 versions')]
  }
}, {
  loader: 'sass-loader',
  options: {
    data: '@import "variables";',
    includePaths: [ res('../static/scss') ]
  }
}];


module.exports = {
  name: 'client',
  target: 'web',
  // devtool: 'source-map',
  entry: {
    bootstrap: [
      res('../static/scss/main.scss'),
    ],
    main: [
      path.resolve(__dirname, '../client/index.js'),
    ],
  },
  output: {
    filename: '[name].[chunkhash].js',
    path: path.resolve(__dirname, '../../dist/buildClient/static'),
    publicPath: '/static/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: ExtractCssChunks.extract({
          use: cssLoaders
        })
      },
    ]
  },
  resolve: {
    alias: {
      'components': res('../shared/components'),
      'containers': res('../shared/containers'),
      'shared': res('../shared'),
      'static': res('../static'),
      'react': 'preact-compat',
      'react-dom': 'preact-compat',
      'preact': path.resolve(__dirname, '../../node_modules', 'preact')
    }
  },
  plugins: [
    new StatsPlugin('stats.json'),
    new ExtractCssChunks(),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['bootstrap'], // needed to put webpack bootstrap code before chunks
      filename: '[name].[chunkhash].js',
      minChunks: Infinity
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
        API_URL: process.env.API_URL ? JSON.stringify(process.env.API_URL) : JSON.stringify('https://rjschakt.se'),
        browser: true
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        screw_ie8: true,
        warnings: false
      },
      mangle: {
        screw_ie8: true
      },
      output: {
        screw_ie8: true,
        comments: false
      },
    }),
    new WebpackBundleSizeAnalyzerPlugin('./plain-report.txt'),
  ]
}
