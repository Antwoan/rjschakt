const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const HappyPack = require('happypack');
const res = p => path.resolve(__dirname, p);

const modeModules = res('../../node_modules');
const entry = res('../server/render.js');
const output = res('../buildServer');

const autoprefixer = require('autoprefixer');
const cssLoaders = [{
  loader: 'css-loader/locals',
  options: {
    modules: true,
    localIdentName: '[name]__[local]--[hash:base64:5]',
    sourceMap: true
  }
}, {
  loader: 'postcss-loader',
  options: {
    plugins: () => [autoprefixer('last 2 versions')],
    sourceMap: 'inline'
  }
}, {
  loader: 'sass-loader',
  options: {
    data: '@import "variables";',
    includePaths: [ res('../static/scss') ],
    sourceMap: true
  }
}]


const externals = fs
  .readdirSync(modeModules)
  .filter(x => !/\.bin|react-universal-component|require-universal-module|webpack-flush-chunks/.test(x))
  .reduce((externals, mod) => {
    externals[mod] = `commonjs ${mod}`
    return externals
  }, {})

module.exports = {
  name: 'server',
  target: 'node',
  devtool: 'source-map',
  // devtool: 'eval',
  entry: {
    // bootstrap: [res('../static/scss/main.scss')],
    main: [entry]
  },
  externals,
  output: {
    path: output,
    filename: '[name].js',
    libraryTarget: 'commonjs2',
    publicPath: '/static/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'happypack/loader?id=js'
      },
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'url-loader?limit=1024'
        // loader: 'url-loader?name=fonts/[name].[ext]'
      },
      {
        test: /\.scss$/,
        exclude: [/node_modules/],
        use: cssLoaders
      }
    ]
  },
  resolve: {
    alias: {
      'components': res('../shared/components'),
      'containers': res('../shared/containers'),
      'shared': res('../shared'),
      'static': res('../static'),
      'react': 'preact-compat',
      'react-dom': 'preact-compat'
    }
  },
  plugins: [
    new HappyPack({
      id: 'js',
      threads: 4,
      loaders: ['babel-loader']
    }),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        API_URL: JSON.stringify('http://localhost:3000'),
        browser: false
      }
    })
  ]
}
