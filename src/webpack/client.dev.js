const path = require('path');
const webpack = require('webpack');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const HappyPack = require('happypack');
const autoprefixer = require('autoprefixer');
const WebpackBundleSizeAnalyzerPlugin = require('webpack-bundle-size-analyzer').WebpackBundleSizeAnalyzerPlugin;
const res = p => path.resolve(__dirname, p);

console.log("process.env.API_URL:", process.env.API_URL);
const cssLoaders = [{
  loader: 'css-loader',
  options: {
    modules: true,
    localIdentName: '[name]__[local]--[hash:base64:5]',
    sourceMap: true
  }
}, {
  loader: 'postcss-loader',
  options: {
    plugins: () => [autoprefixer('last 2 versions')],
    sourceMap: 'inline'
  }
}, {
  loader: 'sass-loader',
  options: {
    data: '@import "variables";',
    includePaths: [ res('../static/scss') ],
    sourceMap: true
  }
}];

module.exports = {
  name: 'client',
  target: 'web',
  devtool: 'source-map',
  // devtool: 'eval',
  entry: {
    bootstrap: [
      res('../static/scss/main.scss'),
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=2000&reload=false&quiet=false&noInfo=false',
    ],
    main: [
      path.resolve(__dirname, '../client/index.js'),
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=2000&reload=false&quiet=false&noInfo=false',
    ],
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '../buildClient'),
    publicPath: '/static/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'happypack/loader?id=js'
      },
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'url-loader?limit=10240'
        // loader: 'url-loader?name=fonts/[name].[ext]'
      },
      {
        test: /\.scss$/,
        use: ExtractCssChunks.extract({
          use: cssLoaders
        })
      }
    ]
  },
  resolve: {
    alias: {
      'components': res('../shared/components'),
      'containers': res('../shared/containers'),
      'shared': res('../shared'),
      'static': res('../static'),
      'react': 'preact-compat',
      'react-dom': 'preact-compat',
      'preact-compat': 'preact-compat/dist/preact-compat',
      'preact': path.resolve(__dirname, '../../node_modules', 'preact')
    }
  },
  plugins: [
    new ExtractCssChunks(),
    new HappyPack({
      id: 'js',
      threads: 4,
      loaders: ['babel-loader']
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['bootstrap'], // needed to put webpack bootstrap code before chunks
      filename: '[name].js',
      minChunks: Infinity
    }),

    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        API_URL: process.env.API_URL ? JSON.stringify(process.env.API_URL) : JSON.stringify('http://rjschakt.dev'),
        browser: true
      }
    })
  ]
}
