import { h, Component }  from 'preact';
import Helmet            from 'preact-helmet';
import Sections          from 'components/Sections/Sections';
import Header            from 'components/Header/Header';
import Details           from 'components/Details/Details';
import getAsyncComponent from 'shared/AsyncComponent';
import PageReducer      from 'components/PageReducer/PageReducer';
import style from './contact.scss';

export default class Contact extends Component {
	constructor(props) {
		super(props);
		props.changeMenuColor('dark');
		if(process.env.browser) {
			// Initialize observers before components register.
			const observer = require('components/Observer/Observer');
			observer.default.init();
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		return (nextProps.transitionStyle !== this.props.transitionStyle && nextProps.transitionStyle.position !== 'relative');
	}

	render({page, transitionStyle, transition}) {
		const Contact = getAsyncComponent(() => import(`components/Contact/Contact`), {
		  resolve: () => require.resolveWeak(`components/Contact/Contact`),
		  chunkName: `components/Contact/Contact`,
		});
		
		return (
			<div class={style.wrapper} style={{  overflow: 'auto', ...transitionStyle }}>
				{ page ? <Helmet
					title={page.post_title}
				/> : null }
				<Contact {...page}/>
			</div>
		);
	}
};