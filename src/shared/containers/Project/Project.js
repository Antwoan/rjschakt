import { h, Component }  from 'preact';
import Helmet            from 'preact-helmet';
import Sections          from 'components/Sections/Sections';
import Header            from 'components/Header/Header';
import Details           from 'components/Details/Details';
import getAsyncComponent from 'shared/AsyncComponent';
import PageReducer      from 'components/PageReducer/PageReducer';
import ErrorComponent from 'components/404/404';
import style from './project.scss';

export default class Project extends Component {
	constructor(props) {
		super(props);
		props.changeMenuColor('light');
		if(process.env.browser) {
			// Initialize observers before components register.
			const observer = require('components/Observer/Observer');
			observer.default.init();
		}
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.url !== this.props.url) {
			this.props.setKey(nextProps.url);
		}
	}

	render({page, transitionStyle, transition}) {
		const project = page;
		const ProjectNavigation = getAsyncComponent(() => import(`components/ProjectNavigation/ProjectNavigation`), {
			resolve: () => require.resolveWeak(`components/ProjectNavigation/ProjectNavigation`),
		  chunkName: `components/ProjectNavigation/ProjectNavigation`,
		});

		if(!project) return (
			<div class={'wrapper wrapper--small ' + style.errorWrapper} style={{ display: 'block', width: '100%', overflow: 'auto', ...transitionStyle }}>
				<ErrorComponent/>
			</div>
		);

		return (
			<div class={style.wrapper} style={{ display: 'block', width: '100%', overflow: 'auto', ...transitionStyle }}>
				<Helmet
					title={page.post_title}
				/>
				{project && project.header ?
					<Header {...project.header} low={true} transition={transition} />
				: null}
				<Details {...project}/>
				{project && project.acf && project.acf.sections ?
					<Sections sections={project.acf.sections}/>
				: null}
				{project ?
					<ProjectNavigation {...project}/>
				: null}

			</div>
		);
	}
};