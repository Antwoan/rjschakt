import { h, Component } from 'preact';
import Helmet           from 'preact-helmet';
import Sections         from 'components/Sections/Sections';

export default class Page extends Component {

	constructor(props) {
		super(props);
		props.changeMenuColor('light');
		if(process.env.browser) {
			// Initialize observers before components register.
			const observer = require('components/Observer/Observer');
			observer.default.init();
		}
	}

	render({page, transitionStyle}) {
		{
			page ? <Helmet
				title={page.post_title}
			/> : null
		}
		return (
			<div style={{ width: '100%', overflow: 'auto', ...transitionStyle }}>
				{page && page.acf && page.acf.sections ?
					<Sections sections={page.acf.sections}/>
				: null}
			</div>
		);
	}
};