import { h, Component }  from 'preact';
import Helmet            from 'preact-helmet';
import Header            from 'components/Header/Header';
import getAsyncComponent from 'shared/AsyncComponent';
import fetch             from 'components/fetch';
import { Link }          from 'preact-router/match';
import AnimatedString    from 'components/AnimatedString/AnimatedString';
import AnimatedBlock     from 'components/AnimatedBlock/AnimatedBlock';
import Image             from 'components/Image/Image';
import style             from './archive.scss';

export default class Archive extends Component {
	state = {
		next_navigation: false,
		previous_navigation: false,
		projects: []
	}

	constructor(props) {
		super(props);
		props.changeMenuColor('dark');
		if(process.env.browser) {
			// Initialize observers before components register.
			const observer = require('components/Observer/Observer');
			observer.default.init();
		}
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.url !== this.props.url) {
			this.props.setKey(nextProps.url);
		}
	}


	render({page, transitionStyle, path, matches, projectInfo, ...props}) {
		if(!page) return null;
		const ProjectNavigation = getAsyncComponent(() => import(`components/ProjectNavigation/ProjectNavigation`), {
		  resolve: () => require.resolveWeak(`components/ProjectNavigation/ProjectNavigation`),
		  chunkName: `components/ProjectNavigation/ProjectNavigation`,
		});
		const info = projectInfo ? projectInfo : {};
		const { previous_navigation, next_navigation, projects } = info;
		const pagenumber = this.props.matches.pagenumber ? Number(this.props.matches.pagenumber) : 1;

		const rootUrl     = path.substring(0, path.lastIndexOf("/") + 1)
		const nextUrl     = rootUrl + (pagenumber + 1);
		const previousUrl = rootUrl + (pagenumber - 1);


		const navigation = {
			previous: previous_navigation ? {
				url: previousUrl,
				post_title: 'Föregående'
			} : false,

			next: next_navigation ? {
				url: nextUrl,
				post_title: 'Nästa'
			} : false
		}

		return (
			<div class={ style.wrapper + ' wrapper'} style={{ overflow: 'auto', ...transitionStyle }}>
				<Helmet
					title={page.post_title}
				/>
				<h1>{page.post_title}</h1>
				{projects ? projects.map(project => (
					<article>
							<Link href={project.url}>
								<div class={'grid grid--medium project-wrapper ' + style.projectWrapper}>
									<Image
										src={project.thumbnail.url}
										width={project.thumbnail.width}
										height={project.thumbnail.height}
									/>
									<div>
										<h2 class={style.projectTitle}><AnimatedString text={project.post_title}/></h2>
										<AnimatedBlock>
										<div class={style.date}><svg class={style.dateIcon} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" /></svg> {project.post_date}</div>
											<p class={style.projectDescription}>{project.excerpt}</p>
										</AnimatedBlock>
										<AnimatedBlock>
											<div class={style.readMoreContainer}>
												<p class={style.readMore}>Läs mer</p>
												<svg class={style.arrow} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 14">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<g stroke-width="2.4" stroke='#112536'>
															<g>
																<path d="M0,7 L36.8677244,7"></path>
																<polyline points="30.0833333 14 37.3579402 6.71178481 30.0833333 0"></polyline>
															</g>
														</g>
													</g>
												</svg>
											</div>
										</AnimatedBlock>
									</div>
							</div>
						</Link>
					</article>
				)) : null}
				<ProjectNavigation {...page} staticNav={navigation}/>
			</div>
		);
	}
};