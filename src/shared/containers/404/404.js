import { h, Component } from 'preact';
import style from './404.scss';
import ErrorComponent from 'components/404/404';

export default class ErrorContainer extends Component {

	constructor(props) {
		super(props);
		props.changeMenuColor('dark');
		if (process.env.browser) {
			const observer = require('components/Observer/Observer');
			observer.default.init();
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
		return (nextProps.transitionStyle !== this.props.transitionStyle && nextProps.transitionStyle.position !== 'relative');
	}


	render({ page, transitionStyle }) {
		return (
			<div class={style.wrapper + ' wrapper wrapper--small'} style={{ overflow: 'auto', ...transitionStyle }}>
				<ErrorComponent/>
			</div>
		);
	}
};