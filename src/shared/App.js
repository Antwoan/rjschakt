import Router           from './TransitionRouter';
// import Router        from 'preact-router';
import AnimatedRoute    from 'components/AnimatedRoute/AnimatedRoute';
import PageReducer      from 'components/PageReducer/PageReducer';
import ViewTransition   from 'components/ViewTransition/ViewTransition';
import { h, Component } from 'preact';
import Page             from 'containers/Page/Page';
import Project          from 'containers/Project/Project';
import Contact          from 'containers/Contact/Contact';
import Archive          from 'containers/Archive/Archive';
import ErrorComponent   from 'containers/404/404';
import Helmet           from 'preact-helmet';
import Menu             from 'components/Menu/Menu';
import Footer           from 'components/Footer/Footer';
import guid             from './guid';
import style from './app.scss';

import globalStyles from 'static/scss/main.scss';

export default class App extends Component {

	state = {
		menuColor: 'light',
		newMenuColor: 'light',
		dynamicKey: this.props.url,
		transitionKey: guid()
	}

	constructor(props) {
		super(props);
		const {initialState: { url, initialData }} = this.props;
		// console.log("initialData", initialData);
		PageReducer.setPage(url, initialData[1]);
		const template = initialData[1] ? initialData[1].template : 404;

		switch (template) {
			case 'page-archive.php':
				this.state.menuColor = 'dark';
				break;

			case 'page-contact.php':
				this.state.menuColor = 'dark';
				break;

			case 404:
				this.state.menuColor = 'dark';
				break;

			default:
				this.state.menuColor = 'light';
				break;
				
		}
	}

	transition = (state) => {
		this.setState({
			transition: state
		});
	}

	changeMenuColor = (newMenuColor) => {
		this.newMenuColor = newMenuColor;
	}

	setMenuColor = (menuColor) => {
		this.setState({
			menuColor
		});
	}

	setDynamicKey = (key) => {
		this.setState({
			dynamicKey: key
		});
	}

	render({ url, initialState: { initialData } }, { transition, menuColor, dynamicKey }) {
		const menu   = initialData[0];
		const page   = initialData[1];
		const routes = initialData[2].map((route, index) => {
			// Add the right component for each route.
			switch(route.template) {

				case 'page-archive.php':
					return {
						...route,
						url: route.url + '/:pagenumber?',
						component: Archive,
						isArchive: true
					}
					break;

				case 'page-contact.php':
					return {
						...route,
						component: Contact,
					}
					break;

				default:
					return {
						...route,
						component: Page
					}
					break;
			}
		});
		
		return (
			<main class={style.wrapper}>
				<Helmet
					title='Schakt & Entreprenad'
					titleTemplate="%s | RJ Schakt & Entreprenad"
					link={[
						{ rel: "canonical", href: process.env.API_URL },
						{
							rel: "apple-touch-icon",
							sizes: "180x180",
							href: "/static/icons/apple-touch-icon.png"
						},
						{
							rel: "icon",
							type: "image/png",
							sizes: "32x32",
							href: "/static/icons/favicon-32x32.png"
						},
						{
							rel: "icon",
							type: "image/png",
							sizes: "16x16",
							href: "/static/icons/favicon-16x16.png"
						},
						{
							rel: "mask-icon",
							color: "#5bbad5",
							sizes: "16x16",
							href: "/static/icons/safari-pinned-tab.svg"
						},
					]}
				/>
				{menu ? <Menu menuColor={menuColor} items={menu}/> : null}

				<Router
					url={url}
					class={style.contentWrapper}>
					{routes.map((route, index) => {
						return(
						<AnimatedRoute
							key={route.isArchive ? dynamicKey + 'archive' : 'route' + index}
							isArchive={route.isArchive}
							RouteComponent={route.component}
							path={route.url}
							transition={this.transition}
							setKey={this.setDynamicKey}
							changeMenuColor={this.changeMenuColor}
						/>
					)})}
					<AnimatedRoute
						RouteComponent={Project}
						path="/projekt/:projekt"
						key={dynamicKey + 'project'}
						setKey={this.setDynamicKey}
						transition={this.transition}
						changeMenuColor={this.changeMenuColor}
					/>
					<AnimatedRoute
						RouteComponent={ErrorComponent}
						type="404"
						key={dynamicKey + 'project'}
						setKey={this.setDynamicKey}
						transition={this.transition}
						changeMenuColor={this.changeMenuColor}
						default
					/>
				</Router>
				<Footer/>
				<ViewTransition
					newMenuColor={this.newMenuColor}
					setMenuColor={this.setMenuColor}
					transition={transition}
				/>
			</main>
		);
	}
}
