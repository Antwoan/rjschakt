import { h } from 'preact';
import Router from 'preact-router';
import TransitionGroup from 'preact-transition-group';

export default class TransitionRouter extends Router {

  componentWillReceiveProps(nextProps) {
    if ('scrollRestoration' in history) {
      history.scrollRestoration = 'manual';
    }
  }

  componentDidMount() {
    super.componentDidMount();
    if ('scrollRestoration' in history) {
      history.scrollRestoration = 'manual';
    }
  }

	componentWillUpdate(nextProps, nextState) {
		super.componentWillUpdate(nextProps, nextState);

    if ('scrollRestoration' in history) {
      history.scrollRestoration = 'manual';
    }
	}

  render(props, state) {
    return (
      <TransitionGroup class={props.class} component="div" transitionName="fade" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
        {super.render(props, state)}
      </TransitionGroup>
    );
  }
}