import { h, Component } from 'preact';
import Loading from 'components/Loading/Loading';
import requireUniversalModule from 'require-universal-module';

export default function createAsyncComponent(asyncComponent, opts) {
  const { ...options } = opts;
  const tools = requireUniversalModule(asyncComponent, options);
  const { requireSync, requireAsync, addModule, mod } = tools;

  let Comp = mod; // initial syncronous require attempt done for us :)


  const Err = () => (
    <p>Error!</p>
  );

  return class AsyncComponent extends Component {
    constructor() {
      super();

      if (!Comp) {
        // try one more syncronous require at render time, in case chunk comes
        // after main.js and isn't available in initial synchronous evaluation
        Comp = requireSync();
      }

      this.state = { hasComponent: !!Comp };
    }

    componentWillMount() {
      addModule(); // record usage of the module for SSR flushing :)

      // the component was successfully synchronously
      // required at one of the last 2 attempts
      if (this.state.hasComponent) return;

      // ok, now it's time to retreive the webpack chunk asynchronously
      requireAsync()
        .then(mod => {
          Comp = mod; // for HMR updates component must be in closure, not state
          this.setState({ hasComponent: !!Comp }); // trigger component re-render
        })
        .catch(error => setState({ error }));
    }

    render() {
      const { hasComponent, error } = this.state;
      if (error) {
        return <Err {...props} error={error} />;
      }
      const props = this.props;
      return hasComponent ? <Comp {...props} /> : <Loading {...props} />;
    }
  }
}