import {h, Component} from 'preact';
import style          from 'components/DualImages/dual-images.scss';
import Image          from 'components/Image/Image';

export default class SingleImage extends Component {
	render({image}) {
		return (
			<section class={'wrapper ' + style.wrapper}>
				<div class={'grid grid__medium'}>
					{image ?
						<div>
							<Image src={image.url} width={image.width} height={image.height} animation='fade'/>
						</div>
					: null}
				</div>
			</section>
		);
	}
}