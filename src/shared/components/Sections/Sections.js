import {h, Component} from 'preact';
import getAsyncComponent from 'shared/AsyncComponent';
import HTML from 'components/HTML/HTML';
import Header from 'components/Header/Header';
import style from 'components/Sections/sections.scss';

export default class Sections extends Component {
		// Stop re-rendering of the sections container to remove layout changes on route change
	shouldComponentUpdate(nextProps, nextState) {
		return false;
	}

	shouldComponentUpdate(nextProps, nextState) {
		// return this.props.sections !== nextProps.sections;
		return false;
	}

	// Todo: Dynamic imports.
	render({ sections }) {
		return (
			<div>
				{ sections.map(section => {
						switch(section.acf_fc_layout) {
							case 'header':
								return <Header {...section}/>
								break;
				}})}
				<div class={style.content}>
					{ 
						sections.map(section => {
							switch(section.acf_fc_layout) {

								case 'content':
									const Content = getAsyncComponent(() => import(`components/Content/Content`), {
									  resolve: () => require.resolveWeak(`components/Content/Content`),
									  chunkName: `components/Content/Content`,
									});
									return <Content {...section}/>;
									break;

								case 'horizontalText':
									const HorizontalText = getAsyncComponent(() => import(`components/HorizontalText/HorizontalText`), {
									  resolve: () => require.resolveWeak(`components/HorizontalText/HorizontalText`),
									  chunkName: `components/HorizontalText/HorizontalText`,
									});
									return <HorizontalText {...section}/>;
									break;

								case 'latestProjects':
									const LatestProjects = getAsyncComponent(() => import(`components/LatestProjects/LatestProjects`), {
									  resolve: () => require.resolveWeak(`components/LatestProjects/LatestProjects`),
									  chunkName: `components/LatestProjects/LatestProjects`,
									});
									return <LatestProjects {...section}/>;
									break;

								case 'services':
									const Services = getAsyncComponent(() => import(`components/Services/Services`), {
									  resolve: () => require.resolveWeak(`components/Services/Services`),
									  chunkName: `components/Services/Services`,
									});
									return <Services {...section}/>;
									break;

								case 'dual_images':
									const DualImages = getAsyncComponent(() => import(`components/DualImages/DualImages`), {
									  resolve: () => require.resolveWeak(`components/DualImages/DualImages`),
									  chunkName: `components/DualImages/DualImages`,
									});
									return <DualImages {...section}/>;
									break;

								case 'single_image':
									const SingleImage = getAsyncComponent(() => import(`components/SingleImage/SingleImage`), {
									  resolve: () => require.resolveWeak(`components/SingleImage/SingleImage`),
									  chunkName: `components/SingleImage/SingleImage`,
									});
									return <SingleImage {...section}/>;
									break;

								case 'contact':
									const Contact = getAsyncComponent(() => import(`components/Contact/Contact`), {
									  resolve: () => require.resolveWeak(`components/Contact/Contact`),
									  chunkName: `components/Contact/Contact`,
									});
									return <Contact {...section}/>;
									break;
					
							}}
						)
					}
				</div>
				
			</div>
		);
	}
}