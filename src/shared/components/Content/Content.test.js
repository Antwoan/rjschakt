import { h } from 'preact';
import Content from './Content.js';
import {deep} from 'preact-render-spy';

const props = {
	content: `<h1>Test</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut obcaecati aperiam nulla recusandae distinctio alias tempore odit ut architecto, beatae delectus esse odio adipisci, qui, optio quae iusto quibusdam. Accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, autem!</p>`
};

test('It did not explode...', () => {
	const context = deep(<Content {...props}/>);
	expect(context.length).toBe(1);
});
