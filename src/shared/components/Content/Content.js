import {h} from 'preact';
import HTML from 'components/HTML/HTML';
import style from './content.scss';

export default ({ content }) => (
	<section class="section">
		<div class={'wrapper wrapper--small ' + style.content}>
			<HTML html={content}/>
		</div>
	</section>
)