import {h, Component} from 'preact';
import style          from './details.scss';
import AnimatedBlock  from 'components/AnimatedBlock/AnimatedBlock';
import AnimatedString from 'components/AnimatedString/AnimatedString';
import HTML           from 'components/HTML/HTML';

export default class Details extends Component {
	render({ post_date, post_title, type, place, acf }) {
		const date = post_date ? `${post_date.month} ${post_date.year}` : '';
		return (
			<article class={style.wrapper + ' section'}>
				<div class="wrapper">
					<div class="grid grid--medium">
						<div class={style.description}>
							<h1 class={style.title}><AnimatedString text={post_title}/></h1>
							{acf ?
								<AnimatedBlock><HTML html={acf.description}/></AnimatedBlock>
							: null}
						</div>
						<div>
							<h2 class={style.date}><AnimatedString text={date}/></h2>
							<div class={style.info}>
								{type ?
									<AnimatedBlock>
										<ul class={style.infoList}>
											<li class={style.infoTitle}>Kategori</li>
											{type.map(name => (
												<li>{name}</li>
											))}
										</ul>
									</AnimatedBlock>
								: null}
								{place ?
									<AnimatedBlock>
										<ul class={style.infoList}>
											<li class={style.infoTitle}>Plats</li>
											{place.map(name => (
												<li>{name}</li>
											))}
										</ul>
									</AnimatedBlock>
								: null}
							</div>
						</div>
					</div>
				</div>
			</article>
		)
	}
}