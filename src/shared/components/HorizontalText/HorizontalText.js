import {h}            from 'preact';
import HTML           from 'components/HTML/HTML';
import AnimatedBlock  from 'components/AnimatedBlock/AnimatedBlock';
import AnimatedString from 'components/AnimatedString/AnimatedString';
import style from './horizontal-text.scss';

export default ({ title, content }) => (
	<section class="section">
		<div class="wrapper wrapper--medium">
			<div class={'grid grid--medium'}>
				<h1 class={style.title}>
					<AnimatedString text={title}/>
				</h1>
				<AnimatedBlock>
					<HTML class={style.text} html={content}/>
				</AnimatedBlock>
			</div>
		</div>
	</section>
)