import { h }    from 'preact';
import { Link } from 'preact-router/match';
import style    from './button.scss';

export default class Button {
	render({text, href, hasFill, cb}) {
		return (
			<Link class={hasFill ? style.button : style.noFill} href={href}>
				{text}
				<svg class={style.arrow} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38 14">
			    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		        <g stroke-width="2.4" stroke={hasFill ? '#FFFFFF' : '#FEBB00'}>
	            <g>
                <path d="M0,7 L36.8677244,7"></path>
                <polyline points="30.0833333 14 37.3579402 6.71178481 30.0833333 0"></polyline>
	            </g>
		        </g>
			    </g>
				</svg>
			</Link>
		);
	}
}