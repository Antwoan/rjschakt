import {h, Component} from 'preact';
import style          from './image.scss';
import observer       from 'components/Observer/Observer';
import HTML           from 'components/HTML/HTML';

export default class Image extends Component {
	state = {
		loaded: false,
		visible: false,
		registered: false,
		animateIndex: null,
		preloadIndex: null
	}

	constructor(props) {
		super(props);
	}

	animate = () => {
		this.setState({
			visible: true
		});
	}

	preload = () => {
		this.image = new Image();
		this.image = this.props.src;
		this.setState({
			loaded: true
		});
	}

	componentDidMount() {
		this.register(this.image);
	}

	componentWillUpdate(nextProps, nextState) {
		// console.log("nextState === this.state:", nextState === this.state);
	}

	register(image) {
		if(!this.state.animateIndex && !this.state.preloadIndex) {
			this.registered = true;
			const animateIndex = observer.register({
				type: 'animated',
				element: image,
				callback: this.animate.bind(this)
			});

			const preloadIndex = observer.register({
				type: 'preloaded',
				element: image,
				callback: this.props.callback ? this.props.callback : this.preload.bind(this)
			});

			this.setState({
				animateIndex,
				preloadIndex
			});
		}
	}

	componentWillUnmount() {
		observer.deregister({
			element: this.image,
			type: 'animated',
			id: this.state.animteIndex,
		});
		observer.deregister({
			element: this.image,
			type: 'preloaded',
			id: this.state.preloadIndex,
		});
	}

	render({ src, width, height, className }, { visible, loaded, animateIndex, preloadIndex }) {
		const paddingBottom = height && width ? height / width * 100 + '%' : false;
		const containerClass = this.props.animation ? style[this.props.animation + 'Container'] : style.slideContainer;
		const visibleClass = this.props.animation ? style[this.props.animation] : style.slide;

		const stateClass = visible ? visibleClass : containerClass;

		return (
			<figure class={className} data-preload-index={preloadIndex} data-animate-index={animateIndex} data-animate-callback={this.animate.bind(this)} ref={(image => {
				this.image = image;
			})}>
				<div style={paddingBottom ? { paddingBottom } : ''} class={stateClass}>
					<img style={{ position: width && height ? 'absolute' : 'relative' }} class={style.image} src={loaded ? src : null} />
					<noscript>
						<div class={style.noJSImage} style={{ backgroundImage: `url(${src})` }}/>
					</noscript>
				</div>
			</figure>
		)
	}
}