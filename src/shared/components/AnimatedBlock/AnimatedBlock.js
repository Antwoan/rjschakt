import {h, Component}   from 'preact';
import {Motion, spring} from 'preact-motion';
import style            from './animated-block.scss';
import observer         from 'components/Observer/Observer';

export default class AnimatedBlock extends Component {

	springSettings = { stiffness: 100, damping: 30 }
	opacitySpringSettings = { stiffness: 20, damping: 12 }

	constructor(props) {
		super(props);
		const defaultStyle = {
			y: 6,
			opacity: 0
		};
		this.state = defaultStyle;
	}

	register(element) {
		if(!this.registered) {
			this.registered = true;
			const animateIndex = observer.register({
				type: 'animated',
				element,
				callback: this.animate.bind(this)
			});

			this.setState({
				animateIndex
			});
		}
	}

	componentDidMount() {
		this.register(this.el);
	}

	animate = () => {
		this.setState({
			y: 0,
			opacity: 1
		});
	}

	render({ children, ...props }, { animateIndex, ...state}) {
		return (
			<Motion style={{ opacity: spring(state.opacity, this.opacitySpringSettings), y: spring(state.y, this.springSettings) }}>
				{({opacity, y}) => 
					<div
						{...props}
						class={style.wrapper + ' ' + props.className}
						data-animate-index={animateIndex}
						ref={el => { this.el = el; }}
						style={{
							opacity,
							WebkitTransform: `translate3d(0, ${y}rem, 0)`,
							transform: `translate3d(0, ${y}rem, 0)`
						}}
					>
						{children}
					</div>
				}
			</Motion>
		);
	}
}
