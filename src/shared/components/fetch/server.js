module.exports = function(url, opts) {
	return require('node-fetch')(url.replace(/^\/\//g,'https://'), opts);
}