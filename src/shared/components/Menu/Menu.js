import { h, Component } from 'preact';
import { Link }         from 'preact-router/match';
import style            from './menu.scss';
import Helmet           from 'preact-helmet';

export default class Menu extends Component {
	state = {
		checked: false,
		smallSrc: '',
		largeSrc: ''
	}

	closeMenu = () => {
		this.setState({
			checked: false
		});
	}

	componentDidMount() {
		this.setState({
			smallSrc: '/static/images/logo_dark.svg',
			largeSrc: '/static/images/logo_light.svg'
		});
	}

	render({items, menuColor}, {checked, smallSrc, largeSrc}) {
		
		const menuClass = menuColor === 'dark' ? style.dark : style.logo;
		return (
			<nav class={style.menu}>
				<Helmet
					link={[{
						rel: 'preload',
						href: '/static/images/logo_dark.svg',
						as: 'image'
					}]}
					link={[{
						rel: 'preload',
						href: '/static/images/logo_light.svg',
						as: 'image'
					}]}
				/>
				<input class={style.input} id="menu-trigger" type="checkbox" checked={checked}/>
				<label for="menu-trigger" class={style.trigger}>
					<span class={style.bar}></span>
					<span class={style.bar}></span>
					<span class={style.bar}></span>
				</label>

				<div class={style.wrapper}>
					<Link class={menuClass} onClick={() => this.closeMenu()} href="/">
					{/*
						<img class={style.logoLarge} src={largeSrc}/>
						*/}
					</Link>
					<div class={style.links}>
						{items.map(item => (
							<Link onClick={() => this.closeMenu()} class={style.link} href={item.url}>{item.title}</Link>
						))}
					</div>
				</div>
			</nav>
		);
	}
}