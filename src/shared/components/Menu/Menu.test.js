import { h } from 'preact';
import Menu from './Menu.js';
import {deep} from 'preact-render-spy';

const props = {
	items: [
		{title: "Hem", slug: "home", url: "/"},
		{title: "Tjänster", slug: "services", url: "/tjanster"},
		{title: "Om oss", slug: "about", url: "/om-oss"},
		{title: "Arbeten", slug: "about", url: "/arbeten"},
		{title: "Kontakt", slug: "kontakt", url: "/kontakt"}
	]
}

test('It did not explode...', () => {
	const context = deep(<Menu {...props}/>);
	expect(context.length).toBe(1);
});
