// import { Motion, spring } from 'preact-motion';
import PageReducer      from 'components/PageReducer/PageReducer';
import { h, Component } from 'preact';
import fetch            from 'components/fetch';

export default class AnimatedRoute extends Component {
	transitionDuration = 700
	projects = {}

	constructor(props) {
		super(props);
		const {url} = props;
		this.state.page = PageReducer.getPage(url);
	}

	componentWillEnter(callback) {
		this.setState({
			transitionStyle: {
				opacity: 0,
				height: 0,
				position: 'absolute',
				zIndex: 1,
				display: 'none'
			}
		});

		const {isArchive} = this.props;

  	if(isArchive) {
			this.fetchProjects(callback);
			
  	} else {
    	callback();
  	}
	}

	componentWillAppear(callback) {
		const {isArchive} = this.props;

  	if(isArchive) {
			this.fetchProjects(callback);
			
  	} else {
    	callback();
  	}
	}

	fetchProjects(callback) {
		const {url, isArchive} = this.props;

		return new Promise(resolve => {
			// Get page before removing transition block
			const pagenumber = this.props.matches.pagenumber ? Number(this.props.matches.pagenumber) : 1;

			fetch(`${process.env.API_URL}/api/archive?page=${pagenumber}`)
		    .then(projects => projects.json())
		    .then(projects => {
		    	this.projectInfo = projects;
		    	callback();
		    	resolve();
		    });
		});
	}

	componentDidAppear() {
		this.setState({});
	}

	componentDidEnter() {
		const {url, isArchive} = this.props;

		const fetchPage = new Promise(resolve => {
			// Get page before removing transition block
			if(!this.state.page) {
				fetch(`${process.env.API_URL}/api/page?url=${url}`)
			    .then(res => res.json())
			    .then(res => {
			    	PageReducer.setPage(url, res);

			    	this.setState({
			    		page: res,
			    	});
		    		resolve();

			    });
			} else {
				resolve();
			}
		});

		const animationEnd = new Promise(resolve => {
			// Wait until first part of animation is done
			setTimeout(() => {
				this.setState({
					transitionStyle: {},
					loading: false
				});
				resolve();
			}, this.transitionDuration);
		});

		// Wait until everything is done. Then make transition block leave and finally reset it
		Promise.all([fetchPage, animationEnd]).then(() => {
			window.scrollTo(0, 0);
			this.props.transition('LEAVE');
			setTimeout(() => {
				this.props.transition('IDLE');
			}, this.transitionDuration);
		});
	}

	componentWillLeave(callback) {
		this.props.transition('ENTER');
		this.setState({
			transitionStyle: {
				zIndex: 20,
				position: 'relative'
			}
		});
		setTimeout(() => {
			callback();
		}, this.transitionDuration);
	}

	render({ RouteComponent, ...props }, state) {
		return (
			<RouteComponent {...state} {...props} projectInfo={this.projectInfo} />
		);
	}
}