// import { Motion, spring } from 'preact-motion';
import PageReducer      from 'components/PageReducer/PageReducer';
import { h, Component } from 'preact';
import fetch            from 'components/fetch';

export default class AsyncRoute extends Component {
	constructor(props) {
		super(props);
		const {url} = props;
		this.state.page = PageReducer.getPage(url);
	}

	componentWillEnter(callback) {
		const {url} = this.props;
		this.setState({
			transitionStyle: {
				opacity: 0,
				height: 0,
				position: 'absolute',
				zIndex: 1
			}
		});
		this.props.transition.leave(new Promise((resolve) => {
			fetch(`${process.env.API_URL}/api/page?url=${url}`)
		    .then(res => res.json())
		    .then(res => {
		    	PageReducer.setPage(url, res);
		    	this.setState({
		    		page: res
		    	});
		    	resolve();
		    });
		}));
		callback();
	}

	componentDidEnter() {
		this.setState({
			transitionStyle: {}
		});
	}

	componentWillLeave(callback) {
		this.setState({
			transitionStyle: {
				zIndex: 20,
				position: 'relative'
			}
		});
		this.props.transition.enter(callback);
	}

	// componentDidMount() {
	// 	const {url} = this.props;
	// 	if(!this.state.page) {
	// 		fetch(`${process.env.API_URL}/api/page?url=${url}`)
	// 	    .then(res => res.json())
	// 	    .then(res => {
	// 	    	PageReducer.setPage(url, res);
	// 	    	this.setState({
	// 	    		page: res
	// 	    	});
	// 	    });
	// 	}
	// }

	render({ Component, ...props }, state) {
		return (
			<Component {...state} {...props} />
		);
	}
}