import {h}            from 'preact';
import style          from './services.scss';
import AnimatedBlock  from 'components/AnimatedBlock/AnimatedBlock';
import AnimatedString from 'components/AnimatedString/AnimatedString';
import Image          from 'components/Image/Image';
import Button         from 'components/Button/Button';

const LatestProjects = ({ services }) => {
	return (
		<section class={'grid grid--medium section'}>
			{services.map(service => (
				<div class={style.service}>
					<div class={style.content}>
						<Image className={style.icon} src={service.icon} animation='fade'/>
						<h2><AnimatedString text={service.title}/></h2>
						<AnimatedBlock>
							<p>{service.description}</p>
						</AnimatedBlock>
						{/*}
						<AnimatedBlock>
							<Button href={'/'.replace(/\/$/, '')} text="läs mer"/>
						</AnimatedBlock>
						{*/}
					</div>
				</div>
			))}
		</section>
	);
};

export default LatestProjects