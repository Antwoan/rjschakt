import { h, Component } from 'preact';
import style from './footer.scss';
import Image from 'components/Image/Image';
import { Link } from 'preact-router/match';

export default class Footer extends Component {
	render() {
		return (
			<footer class={style.footer}>
				<div class={style.wrapper}>
					<div class={style.grid + ' grid grid--medium'}>
						<Link href="/">
							<Image
								src={'/static/images/logo_light.svg'}
								animation='fade' />
						</Link>
						<div class={style.contactInfo}>
							<p>tel: <a href="tel:+46 73-637 57 52">+46 73-637 57 52</a></p>
							<p>e-post: <a href="mailto:robin@rjschakt.se">robin@rjschakt.se</a></p>
						</div>
					</div>
				</div>
			</footer>
		)
	}
}