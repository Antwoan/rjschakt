import { h, Component } from 'preact';
import style from './404.scss';
import { Link } from 'preact-router/match';
import AnimatedString from 'components/AnimatedString/AnimatedString';
import AnimatedBlock from 'components/AnimatedBlock/AnimatedBlock';
import Helmet from 'preact-helmet';

export default class ErrorComponent extends Component {
	shouldComponentUpdate() {
		return false;
	}

	render() {
		return (
			<div class={'grid grid--medium align-middle ' + style.grid}>
				<Helmet
					title="Sidan kunde inte hittas."
				/>
				<h1 class={style.title}><span class={style.error}><AnimatedString className="first" text='404' /></span><AnimatedString text='Sidan kunde inte hittas.' /></h1>
				<AnimatedBlock>
					<p class={style.preamble}>
						Tyvärr kunde vi inte hitta sidan du söker. Prova att navigera med hjälp av menyn ovan eller gå till <Link href='/'>startsidan</Link>.
						</p>
				</AnimatedBlock>
			</div>
		)
	}
}