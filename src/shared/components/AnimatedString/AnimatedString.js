import {h, Component}            from 'preact';
import {StaggeredMotion, spring} from 'preact-motion';
import style                     from './animated-string.scss';
import observer                  from 'components/Observer/Observer';

export default class AnimatedString extends Component {

	springSettings = { stiffness: 400, damping: 50 }

	constructor(props) {
		super(props);
		const defaultStyles = {
			y: 1.5,
			r: 4
		};
		this.state = defaultStyles;
		this.defaultStyles = defaultStyles;
	}

	register(element) {
		if(!this.registered) {
			this.registered = true;
			const animateIndex = observer.register({
				type: 'animated',
				element,
				callback: this.animate.bind(this)
			});

			this.setState({
				animateIndex
			});
		}
	}

	componentDidMount() {
		if(this.props.text && this.el)
			this.register(this.el);
	}

	componentDidUpdate() {
		if(this.props.text && this.el)
			this.register(this.el);
	}

	componentWillReceiveProps({ text }) {
		if(text !== this.props.text) {
			console.log("this:", this);
			this.forceUpdate();
		}
	}

	animate = () => {
		setTimeout(() => {
			this.setState({
				y: 0,
				r: 0
			});
		}, 300);
	}

	getStyles = (prevStyles) => {
    const endValue = prevStyles.map((_, i) => {
      return i === 0
        ? {
        	y: spring(this.state.y, this.springSettings),
        	r: spring(this.state.r, this.springSettings)
        }
        : {
        	y: spring(prevStyles[i - 1].y, this.springSettings),
        	r: spring(prevStyles[i - 1].r, this.springSettings)
        };
    });
    return endValue;
	}

	render({ text }, { animateIndex, ...state}) {
		if(!text)
			return null;
		const splitText = text.split(' ');
		const wordStyles = splitText.map(word => ({
			word,
			...this.defaultStyles
		}));

		return (
			<StaggeredMotion defaultStyles={wordStyles} styles={this.getStyles}>
				{ words => 
					<span class={style.string} data-animate-index={animateIndex} ref={el => { this.el = el; }}>
						{words.map(({word, y, r}, i) => 
							<span key={i} class={style.wrapper}>
								<span class={style.word} style={{
									WebkitTransform: `translate3d(0, ${y}em, 0) rotate(${r}deg)`,
									transform: `translate3d(0, ${y}em, 0) rotate(${r}deg)`
								}}>
									{ i === splitText.length - 1 ? splitText[i] : [splitText[i], <span>&nbsp;</span>] }
								</span>
							</span>
						)}
					</span>
				}
			</StaggeredMotion>
		);
	}
}
