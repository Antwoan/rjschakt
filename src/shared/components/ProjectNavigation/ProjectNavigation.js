import {h, Component} from 'preact';
import { Link }       from 'preact-router/match';
import style          from './project-navigation.scss';
import AnimatedBlock  from 'components/AnimatedBlock/AnimatedBlock';
// import AnimatedString from 'components/AnimatedString/AnimatedString';

export default class ProjectNavigation extends Component {
	render({ staticNav, post_date, projects, ID, post_title, type, place, acf }) {
		let nextProject = false;
		let previousProject = false;


		if(projects && !staticNav) {
			const count = projects.length;
			projects.map((project, index) => {
				if(ID === project.ID) {
					// is last project?
					nextProject = index === count - 1 ? false : projects[index + 1];

					// is first project?
					previousProject = index === 0 ? false : projects[index - 1];
				}
			});
		}

		if(staticNav) {
			previousProject = staticNav.previous;
			nextProject = staticNav.next;
		}

		return (
			<div class={style.wrapper}>
				<div class={'wrapper'}>
					<nav class={'grid'}>
						<div>
							{ previousProject ?
								<Link class={style.previousLink} href={previousProject.url}>
									<div class={style.previousArrow}>
										<span class={style.firstBlade}></span>
										<span class={style.lastBlade}></span>
										<span class={style.stem}></span>
									</div>
									<h4 class={style.previousTitle}>{previousProject.post_title}</h4>
								</Link>
							: null }
						</div>
						<div>
							{ nextProject ?
								<Link class={style.nextLink} href={nextProject.url}>
									<h4 class={style.nextTitle}>{nextProject.post_title}</h4>
									<div class={style.nextArrow}>
										<span class={style.firstBlade}></span>
										<span class={style.lastBlade}></span>
										<span class={style.stem}></span>
									</div>
								</Link>
							: null }
						</div>
					</nav>
				</div>
			</div>
		)
	}
}