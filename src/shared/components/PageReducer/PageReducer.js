import { h, Component } from 'preact';
class PageReducer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pages: {}
		};
	}
	setPage(url, page) {
		this.setState({
			pages: {
				...this.state.pages,
				[url]: page
			}
		});
	}
	getPage(url) {
		return this.state.pages[url];
	}
	getPages() {
		return this.state.pages;
	}
}

const pageReducer = new PageReducer();

export default pageReducer