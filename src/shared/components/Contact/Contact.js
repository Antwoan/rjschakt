import { h, Component } from 'preact';
import HTML from 'components/HTML/HTML';
import style from './contact.scss';
import AnimatedString from 'components/AnimatedString/AnimatedString';

export default class Contact extends Component {
	shouldComponentUpdate() {
		return false;
	}
    
    render({ acf }) {
        if (typeof acf === 'undefined') {
            return null;
        }

        const { title, phone, email, text } = acf;
        return (
            <section class={style.wrapper + ' wrapper wrapper--small'}>

                <div class={style.container}>
                    <span class={style.preTitle}><AnimatedString text='Kontakt' /></span>
                    <div class={style.titleContainer}>
                        {title ?
                            <h1 class={style.title}><AnimatedString text={title} /></h1>
                            : null
                        }
                    </div>

                    <div class="grid grid--medium">
                        <div class={style.content}>
                            <HTML html={text} class={style.text} />
                        </div>
                        <div class={style.content}>
                            { phone ? <h2 class={style.subTitle}>Telefon</h2> : null }
                            { phone ? <a class={style.link} href={`tel:${phone}`}>{phone}</a> : null }
                            
                            { email ? <h2 class={style.subTitle}>E-post</h2> : null }
                            { email ? <a class={style.link} href={`mailto:${email}`}>{email}</a> : null }
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}