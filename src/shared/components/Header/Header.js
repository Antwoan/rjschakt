import {h, Component} from 'preact';
import HTML           from 'components/HTML/HTML';
import style          from './header.scss';
import scrollStyles          from './scroll.scss';
import Button         from 'components/Button/Button';
import observer       from 'components/Observer/Observer';
import Helmet         from 'preact-helmet';
import AnimatedString from 'components/AnimatedString/AnimatedString';
import AnimatedBlock  from 'components/AnimatedBlock/AnimatedBlock';
import imagesLoaded   from 'imagesloaded';

export default class Header extends Component {
	registered = false
	transitionDuration = 450

	state = {
		registered: false,
		trigger: 0,
		transform: `translate3d(0, 0, 0)`,
		imageLoaded: false
	}

	parallax() {
		const height = window.innerHeight;
		const lastPosition = window.scrollY;
		const offset = Math.round(lastPosition / height * 100) * -2;
		const opacity = (height - lastPosition) / height;

		if(this.backgroundEl){
			this.backgroundEl.style.msTransform = `translate3d(0, ${offset}px, 0)`;
			this.backgroundEl.style.webkitTransform = `translate3d(0, ${offset}px, 0)`;
			this.backgroundEl.style.transform = `translate3d(0, ${offset}px, 0)`;

			if(lastPosition > height + 30) {
				this.backgroundEl.style.opacity = 0;
			} else {
				this.backgroundEl.style.opacity = 1;
			}
		}

		if(this.bottomRow){
			this.bottomRow.style.msTransform = `translate3d(0, ${offset * 0.2}px, 0)`;
			this.bottomRow.style.webkitTransform = `translate3d(0, ${offset * 0.2}px, 0)`;
			this.bottomRow.style.transform = `translate3d(0, ${offset * 0.2}px, 0)`;
			this.bottomRow.style.opacity = opacity;
		}

		if(this.contentEl){
			this.contentEl.style.msTransform = `translate3d(0, ${offset * 0.9}px, 0)`;
			this.contentEl.style.webkitTransform = `translate3d(0, ${offset * 0.9}px, 0)`;
			this.contentEl.style.transform = `translate3d(0, ${offset * 0.9}px, 0)`;
			this.contentEl.style.opacity = opacity;
		}

		if(this.titleEl){
			this.titleEl.style.msTransform = `translate3d(0, ${offset * 1.5}px, 0)`;
			this.titleEl.style.webkitTransform = `translate3d(0, ${offset * 1.5}px, 0)`;
			this.titleEl.style.transform = `translate3d(0, ${offset * 1.5}px, 0)`;
			this.titleEl.style.opacity = opacity;
		}
	}

	inView(entry) {
		this.isIntersecting = entry.isIntersecting;
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.background.url !== this.props.background.url) {
			imagesLoaded(this.imageEl, () => {
				if(this.props.transition) {
					this.props.transition('LEAVE');
					setTimeout(() => {
						this.props.transition('IDLE');
					}, this.transitionDuration);
				}
				this.setState({ imageLoaded: true });
			});
		}
	}

	componentDidMount() {
		imagesLoaded(this.imageEl, () => {
			this.setState({ imageLoaded: true });
		});
		this.parallax();
		if(this.props.low)
			this.setState({ loaded: true });

		window.addEventListener('scroll', () => window.requestAnimationFrame(() => {
			this.parallax();
		}), Modernizr.passiveeventlisteners ? {passive: true} : false);
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', () => window.requestAnimationFrame(() => {
			this.parallax();
		}), Modernizr.passiveeventlisteners ? {passive: true} : false);
	}

	register(backgroundEl) {
		if(!this.registered){
			this.registered = true;
		}
	}


	render({ background, content, title, has_button, button_text, link, low }, { animateIndex, parallax, loaded, imageLoaded }) {
			// <div class={style.background} style={parallax} ref={backgroundEl => { this.backgroundEl = backgroundEl; }}>
			// 	<img src={background.url}/>
			// </div>
			// console.log("background:", background);
		if(has_button) {
			var reg = /.+?\:\/\/.+?(\/.+?)(?:#|\?|$)/;
			var href = reg.exec(link) ? reg.exec(link)[1] : '/';
		}

		let wrapperClass = style.wrapper;
		if(low) {
			wrapperClass = style.lowWrapper
		}


		const srcset = background.sizes ? `${background.sizes.header_small} 485w, ${background.sizes.header_medium} 900w, ${background.sizes.header_large} 1800w, ${background.sizes.header_large} 1800w, ${background.sizes.header_full} 2560w` : '';


		return (
			<header class={wrapperClass + ' align-middle'} data-animate-index={animateIndex} ref={el => this.register(el) }>
				{ process.env.NODE_ENV === 'production' ? <Helmet
					link={[{
						rel: 'preload',
						href: background.url,
						as: 'image'
					}]}
				/> : null }

				<div class={imageLoaded ? style.sharp : style.background} style={{ ...parallax, backgroundImage: `url(${background.sizes.base_64})` }} ref={backgroundEl => { this.backgroundEl = backgroundEl; }}>
					<img ref={ imageEl => { this.imageEl = imageEl; } } class={imageLoaded ? style.loaded : style.backgroundImage} src={background.sizes ? background.sizes.header_large : background.url} srcset={srcset}/>
				</div>

				<div class={style.container}>
					<div class={style.titleContainer} ref={titleEl => { this.titleEl = titleEl; }}>
						{	title ? 
							<h1><AnimatedString text={title}/></h1>
							: null
						}
					</div>
					
					<AnimatedBlock>
						<div class={style.content} ref={contentEl => { this.contentEl = contentEl; }}>
								<HTML html={content} class={style.text}/>
								{ has_button ?
									<Button text={button_text} hasFill={false} href={href.replace(/\/$/, '')} />
								 : null}
						</div>
					</AnimatedBlock>
				</div>
				{low ? null :
					<div class={style.bottomRow + ' grid'} ref={bottomRow => { this.bottomRow = bottomRow; }}>
						<div class={style.arrowContainer}>
							<div class={scrollStyles.mouse}/>
							<p class={scrollStyles.text}>Skrolla</p>
						</div>
						<div class={style.contectInfo}>
							<a class={style.mail} href="tel:+46 73-637 57 52"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<g fill="#FFFFFF" fill-rule="nonzero">
										<path d="M476.536,22.921 L382.288,1.18 C360.688,-3.804 338.679,7.365 329.949,27.736 L286.449,129.228 C278.467,147.854 283.845,169.826 299.53,182.658 L339.546,215.399 C311.009,267.774 267.775,311.008 215.399,339.546 L182.658,299.531 C169.826,283.846 147.854,278.468 129.228,286.45 L27.736,329.949 C7.365,338.68 -3.804,360.691 1.18,382.287 L22.922,476.537 C27.74,497.417 46.072,512 67.5,512 C312.347,512 512,313.731 512,67.5 C512,46.071 497.417,27.739 476.536,22.921 Z M69.289,463.996 L48.368,373.327 L146.58,331.236 L202.296,399.33 C301.101,352.977 352.884,301.294 399.332,202.295 L331.235,146.58 L373.327,48.368 L463.996,69.289 C463.049,286.877 286.906,463.044 69.289,463.996 Z"></path>
									</g>
								</g>
							</svg> +46 73-637 57 52</a>
							<a class={style.phone} href="mailto:robin@rjschakt.se"><svg viewBox="0 0 512 384" xmlns="http://www.w3.org/2000/svg">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<g fill="#FFFFFF" fill-rule="nonzero">
										<path d="M464,0 L48,0 C21.49,0 0,21.49 0,48 L0,336 C0,362.51 21.49,384 48,384 L464,384 C490.51,384 512,362.51 512,336 L512,48 C512,21.49 490.51,0 464,0 Z M464,48 L464,88.805 C441.578,107.064 405.832,135.456 329.413,195.295 C312.572,208.542 279.212,240.367 256,239.996 C232.792,240.371 199.421,208.537 182.587,195.295 C106.18,135.465 70.425,107.067 48,88.805 L48,48 L464,48 Z M48,336 L48,150.398 C70.914,168.649 103.409,194.26 152.938,233.044 C174.795,250.249 213.072,288.23 256,287.999 C298.717,288.23 336.509,250.8 359.053,233.052 C408.581,194.269 441.085,168.651 464,150.398 L464,336 L48,336 Z"></path>
									</g>
								</g>
							</svg> robin@rjschakt.se</a>
						</div>
					</div>
				}
			</header>
		)
	}
}