import { h } from 'preact';

const createMarkup = (html) => {
	return { __html: html };
}
export default ({ html, ...props }) => (
	<span {...props} dangerouslySetInnerHTML={createMarkup(html)} />
)