import {h, Component} from 'preact';
import style          from './dual-images.scss';
import Image          from 'components/Image/Image';

export default class DualImages extends Component {
	render({image_1, image_2}) {
		return (
			<section class={'wrapper ' + style.wrapper}>
				<div class={'grid grid--medium'}>
					{image_1 ?
						<div>
							<Image src={image_1.url} width={image_1.width} height={image_1.height} animation='fade'/>
						</div>
					: null}
					{image_2 ?
						<div class={style.lastImage}>
							<Image src={image_2.url} width={image_2.width} height={image_2.height} animation='fade'/>
						</div>
					: null}
				</div>
			</section>
		);
	}
}