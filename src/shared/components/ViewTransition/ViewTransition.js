import style from './view-transition.scss';
import {h, Component} from 'preact';

class ViewTransition extends Component {

	shouldComponentUpdate({ transition }) {
		return transition !== this.props.transition;
	}

	render({ transition, setMenuColor, newMenuColor }) {
		let transitionClass = style.idle;

		switch(transition) {

			case 'IDLE':
				transitionClass = style.idle;
				break;

			case 'ENTER':
				transitionClass = style.enter;
				break;


			case 'COVER':
				transitionClass = style.cover;
				break;

			case 'LEAVE':
				setMenuColor(newMenuColor);
				transitionClass = style.leave;
				break;

			default:
				break;
		}

		return (
			<div>
				<div class={transitionClass} />
				<div class={transitionClass} />
			</div>
		);
	}
}

export default ViewTransition