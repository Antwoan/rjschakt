import {h, Component} from 'preact';
import Button         from 'components/Button/Button';
import Image          from 'components/Image/Image';
import style          from './latest-projects.scss';
import AnimatedBlock  from 'components/AnimatedBlock/AnimatedBlock';
import AnimatedString from 'components/AnimatedString/AnimatedString';
import {Link}         from 'preact-router/match';
import observer       from 'components/Observer/Observer';

export default class LatestProjects extends Component {
	state = {
		projectStates: []
	}

	animate = (index) => {
		const {projectStates} = this.state;
		this.setState({
			projectStates: [
				...projectStates.slice(0, index),
				{
					...projectStates[index],
					buttonClass: style.visibleButton	
				},
				...projectStates.slice(index + 1)
			]
		});
	}

	register(element, index) {
		if(element && this.state.projectStates.length < this.props.projects.length) {
		// 	this.registered = true;
			const animateIndex = observer.register({
				type: 'animated',
				element,
				callback: this.animate.bind(this, index)
			});

			this.setState({
				projectStates: [
					...this.state.projectStates,
					{
						animateIndex,
						buttonClass: style.button
					}
				]
			});
		}
	}

	render({ projects }, { projectStates }) {
		return (
			<section class={style.projects + ' section'}>
				{projects.map((project, index) => (
					<article class={'wrapper ' + style.wrapper}>
						<div class="grid grid--medium">
							<div class={style.image}>
								<Link href={project.url}>
									<Image
										src={project.thumbnail.url}
										width={project.thumbnail.width}
										height={project.thumbnail.height}
									/>
								</Link>
							</div>
							<div class={style.content}>
								<div class={style.textContent}>
									<div class={style.textWrapper}>
										<AnimatedBlock>
											<div class={style.dateContainer}>
												<svg class={style.dateIcon} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z"/></svg>
												<p class={style.date}>{project.post_date}</p>
											</div>
										</AnimatedBlock>
									</div>
									<h2 class={style.title}><Link href={project.url}>
										<AnimatedString text={project.post_title}/>
									</Link></h2>
									<div class={style.textWrapper}>
										<AnimatedBlock>
											<p>{project.excerpt}</p>
										</AnimatedBlock>
										<div
											class={style.buttonContainer}
											ref={ (el) => { this.el = el; this.register(el, index); } }
											data-animate-index={projectStates[index] ? projectStates[index].animateIndex : null}
											className={projectStates[index] ? projectStates[index].buttonClass : null}>

											<Link href={project.url}>
												<svg class={style.arrow} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38 14">
											    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										        <g stroke-width="2.4" stroke={'#FFFFFF'}>
									            <g>
								                <path d="M0,7 L36.8677244,7"></path>
								                <polyline points="30.0833333 14 37.3579402 6.71178481 30.0833333 0"></polyline>
									            </g>
										        </g>
											    </g>
												</svg>
											</Link>

										</div>
									</div>
								</div>
							</div>
						</div>
					</article>
				))}
			</section>
		);
	};
}