import { h, Component } from 'preact';
import guid from 'shared/guid';

class ComponentObserver {
	preloaded = {}
	animated = {}

	handleAnimation = (entries) => {
		entries.map((entry) => {
			const index = entry.target.dataset.animateIndex;
			if(entry.intersectionRatio > 0) {
				this.animated[index].callback(entry);
				this.animator.unobserve(entry.target);
			}
		});
	}

	handlePreload = (entries) => {
		entries.map((entry) => {
			const index = entry.target.dataset.preloadIndex;
			if(entry.intersectionRatio > 0 && !this.preloaded[index].done) {
				this.preloaded[index] = { ...this.preloaded[index], done: true };
				this.preloaded[index].callback();
				this.preloader.unobserve(entry.target);
			}
		});
	}


	init = () => {
		this.preloader = new IntersectionObserver(this.handlePreload, {
			rootMargin: '400px 0px 400px 0px',
			threshold: 0.0001
		});

		this.animator = new IntersectionObserver(this.handleAnimation, {
			rootMargin: '-0px 0px -0px 0px',
			threshold: 0.0001
		});
	}

	observe = (entry) => {
		if(entry.type === 'animated') {
			this.animator.observe(entry.element);
		} else {
			this.preloader.observe(entry.element);
		}
	}

	deregister = (entry) => {
		const {element, id, type} = entry;
		delete this[type][id];

		if(type === 'animated') {
			this.animator.unobserve(element);
		} else {
			this.preloader.unobserve(element);
		}
	}

	destroy = () => {

		Object.keys(this.animated).map(entry => {
			this.animator.unobserve(this.animated[entry].element);
		});

		Object.keys(this.preloaded).map(entry => {
			this.preloader.unobserve(this.preloaded[entry].element);
		});

		this.preloaded = {};
		this.animated = {};

		if(this.animator)
			this.animator.disconnect();

		if(this.preloader)
			this.preloader.disconnect();
	}

	register(entry) {
		// Add entry to observed array
		const id = guid();
		this[entry.type][id] = entry

		this.observe(entry);

		return id;
	}
}

const obs = new ComponentObserver();

export default obs