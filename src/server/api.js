const fetch     = require('node-fetch');
const redis     = require('redis');
const flatten   = require('flat');
const unflatten = require('flat').unflatten;
const redisDeletePattern = require('redis-delete-pattern');

const cache  = redis.createClient('6379', 'redis');

// Get menu from Wordpress and save it in redis
function getMenuFromRemote() {
	return fetch(`http://nginx/wp-json/montana/menu`)
		.then(response => response.json())
		.then(body => {
			const flatBody = JSON.stringify(body);
			cache.set('menu', flatBody);
			return body;
		});
}

// Get menu items from redis, fallback to Wordpress if it doesn't exist.
function getMenu() {
	return new Promise(resolve => {
	  cache.get('menu', function (err, data) {
	    if (err) throw err;

	    if (data != null) {
	      return resolve(JSON.parse(data));
	    } else {
				getMenuFromRemote().then((remoteData) => {
					resolve(remoteData);
				});
	    }
	  });
	});
}

// Get all active routes from Wordpress
function getRoutesFromRemote() {
	return fetch('http://nginx/wp-json/montana/routes')
		.then(response => response.json())
		.then(body => {
			const flatBody = JSON.stringify(body);
			cache.set('registeredPages', flatBody);
			return body;
		});
}

function getRoutes() {
	return new Promise(resolve => {
	  cache.get('registeredPages', function (err, data) {
	    if (err) throw err;

	    if (data != null) {
	      return resolve(JSON.parse(data));
	    } else {
				getRoutesFromRemote().then((remoteData) => {
					resolve(remoteData);
				});
	    }
	  });
	});
}


// Get page from Wordpress and save it in redis
function getPageFromRemote(url) {
	return fetch(`http://nginx/wp-json/montana/page?url=${url}`)
		.then(response => response.json())
		.then(body => {
			const flatBody = JSON.stringify(flatten(body));
			cache.set(url, flatBody);
			return body;
		});
}

// Get archive from Wordpress
function getArchiveFromRemote(page, done) {
	return fetch(`http://nginx/wp-json/montana/archive?page=${page}`)
		.then(response => response.json())
		.then(body => {
			const key = `/projectarchive/page/${page}`;
			const flatBody = JSON.stringify(flatten(body));
			cache.set(key, flatBody, () => {
				cache.expire(key, 60 * 60 * 24 * 31);
				// cache.expire(key, 3);
				if (done)
					return done(body);
			});
		});
}

// Get page from Wordpress and save it in redis
function getAllPagesFromRemote(singleUrl, done) {
	let singlePage = false;

	redisDeletePattern({
		redis: cache,
		pattern: '/projectarchive/page/*'
	}, function handleError(err) {
		console.log("redisDeletePattern err", err);
	});

	cache.dbsize( function (err, numKeys) {
		console.log('Redis DB size:', numKeys);
	});


	return fetch(`http://nginx/wp-json/montana/pages`)
		.then(response => response.json())
		.then(body => {
			body.map(page => {
				const {url} = page;

				if(singleUrl === url) {
					singlePage = page;
				}

				const flatBody = JSON.stringify(flatten(page));
				cache.set(url, flatBody, () => {
					// cache.expire(url, 3);
				});
			});

			if(done)
				return done(singlePage);
		});
}

// Delete page from redis
function deletePage(url) {
	redisDeletePattern({
		redis: cache,
		pattern: '/projectarchive/page/*'
	}, function handleError(err) {
		console.log("redisDeletePattern err", err);
	});
	return cache.del(url);
}

// Check if page exists
function getArchive(page) {
	return new Promise(resolve => {
		const key = `/projectarchive/page/${page}`;
		cache.get(key, function (err, data) {
	    if (err) throw err;

	    if (data != null) {
	    	console.log("Archive from redis!");
	      return resolve(unflatten(JSON.parse(data)));
	    } else {
				new Promise(done => {
					getArchiveFromRemote(page, done).then(() => {
		    		console.log("Archive from remote!");
					});
				}).then(() => {
					cache.get(key, function (err, data) {
						return resolve(unflatten(JSON.parse(data)));
	    		});
				});
	    }
	  });
	});
}

// Check if page exists
function getPage(url) {
	if(url.indexOf('projektarkiv') !== -1) {
		url = '/projektarkiv';
	}
	return new Promise(resolve => {
	  cache.get(url, function (err, data) {
	    if (err) throw err;

	    if (data != null) {
	    	console.log("from redis!");
	      return resolve(unflatten(JSON.parse(data)));
	    } else {
				new Promise(done => {
					getAllPagesFromRemote(url, done).then(() => {
		    		console.log("from remote!");
					});
				}).then(() => {
	    		cache.get(url, function (err, data) {
						return resolve(unflatten(JSON.parse(data)));
	    		});
				});
	    }
	  });
	});
}


// Get all data needed to render 
function getInitData(url) {
	return Promise.all([
		getMenu(),
		getPage(url),
		getRoutes(),
	]);
}

exports.getInitData           = getInitData;
exports.deletePage            = deletePage;
exports.getRoutes             = getRoutes;
exports.getPage               = getPage;
exports.getArchive  				  = getArchive;
exports.getArchiveFromRemote  = getArchiveFromRemote;
exports.getAllPagesFromRemote = getAllPagesFromRemote;
exports.getRoutesFromRemote   = getRoutesFromRemote;
exports.getPageFromRemote     = getPageFromRemote;
exports.getMenuFromRemote     = getMenuFromRemote;