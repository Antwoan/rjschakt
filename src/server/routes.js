const api = require('./api');
module.exports = function(router) {

	router.get('/page', (req, res) => {
		api.getPage(req.query.url).then(data => {
			res.json(data);
		});
	});

	router.get('/archive', (req, res) => {
		api.getArchive(req.query.page).then(data => {
			res.json(data);
		});
	});

};