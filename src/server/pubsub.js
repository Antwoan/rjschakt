const redis = require('redis');
const api   = require('./api');

const client = redis.createClient('6379', 'redis');

client.subscribe('pages/update');
client.subscribe('pages/delete');
client.subscribe('menu/update');
 
client.on('message', function(channel, data){
	console.log("channel:", channel);
	console.log("data:", data);
	if(channel === 'pages/update') {
		// Get new page from remote and save in redis
		// api.getPageFromRemote(data);
		// client.flushall( function (err, succeeded) {
		// 	if(err) {
		// 		console.log("Error when trying to empty Redis DB:", err);
		// 	} else {
		//   	console.log('Success empty Redis DB', succeeded); // will be true if successfull
		// 	}
			api.getMenuFromRemote();
			api.getAllPagesFromRemote();
			api.getRoutesFromRemote();
		// });
	}

	if(channel === 'pages/delete') {
		api.getRoutesFromRemote();
		api.deletePage(data);
		api.getAllPagesFromRemote();
	}

	if(channel === 'menu/update')
		// Get new page from remote and save in redis
		api.getMenuFromRemote();
});