import path                from 'path';
import render              from 'preact-render-to-string';
import { h }               from 'preact';
import App                 from '../shared/App.js';
import { flushChunkNames } from 'require-universal-module/server'
import flushChunks         from 'webpack-flush-chunks';
import Helmet              from 'preact-helmet';
import fetch               from 'node-fetch';
import fs                  from 'fs';

import { getInitData } from './api';

export default ({ clientStats, outputPath }) => (req, res, next) => {
  
  getInitData(req.url).then(data => {
    const status = data[1] ? 200 : 404;
    const initialState = {
      url: req.url,
      initialData: data
    };
    const HTML = render(<App url={req.url} initialState={initialState}/>);
    const head = Helmet.rewind();

    const {
      js,
      styles, // external stylesheets
      // css, // raw css
      cssHash,

      // arrays of file names (not including publicPath):
      scripts,
      stylesheets,

      publicPath
    } = flushChunks(clientStats, {
      chunkNames: flushChunkNames(),
      before: ['bootstrap'],
      after: ['main'],

      // only needed if serving css rather than an external stylesheet
      // note: during development css still serves as a stylesheet
      // outputPath
    });

    let preloadScripts = '';
    let preloadStyles = '';
    if(process.env.NODE_ENV === 'production') {
      scripts.map(script => preloadScripts += `<link rel="preload" href="/static/${script}" as="script">`);

      stylesheets.map(script => preloadStyles += `<link rel="preload" href="/static/${script}" as="style">`);
    }

    const modernizr = require('./modernizr');
    res.status(status).send(
      `<!doctype html>
        <html class="no-js">
          <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="prefetch" href="/static/fonts/roboto/roboto-v16-latin-300.woff2" as="font">
            <link rel="prefetch" href="/static/fonts/roboto/roboto-v16-latin-regular.woff2" as="font">
            <link rel="prefetch" href="/static/fonts/roboto/roboto-v16-latin-700.woff2" as="font">
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KZSF5RB');</script>
            <!-- End Google Tag Manager -->
            ${head.title.toString()}
            ${preloadStyles}
            ${preloadScripts}
            ${styles}
            <script>${modernizr}</script>
            <script>window.state=${JSON.stringify(initialState)};</script>
          </head>
          <body>
          <!-- Google Tag Manager (noscript) -->
          <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZSF5RB"
          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
          <!-- End Google Tag Manager (noscript) -->
            <div id="root">${HTML}</div>
            ${cssHash}
            ${js}
          </body>
        </html>`
    );
  });

}
