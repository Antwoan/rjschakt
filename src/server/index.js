const path                       = require('path');
const express                    = require('express');
const webpack                    = require('webpack');
const webpackDevMiddleware       = require('webpack-dev-middleware');
const webpackHotMiddleware       = require('webpack-hot-middleware');
const webpackHotServerMiddleware = require('webpack-hot-server-middleware');

const bodyParser                 = require('body-parser');
const favicon                    = require('serve-favicon');

const clientConfig               = require('../webpack/client.dev');
const clientProdConfig           = require('../webpack/client.prod');
const serverConfig               = require('../webpack/server.dev');

const DEV                        = process.env.NODE_ENV === 'development';
const publicPath                 = clientConfig.output.publicPath;
const outputPath                 = clientConfig.output.path;
const app                        = express();

const router = express.Router();

app.use('/static', express.static(path.join(__dirname, '..', 'static')))

// Stop requests for favicon going to router
app.use(favicon(path.join(__dirname, '..', 'static', 'icons', 'favicon.ico')));

require('./routes')(router);

require('./pubsub');

app.use('/api', router);

if (DEV) {
  const multiCompiler = webpack([clientConfig, serverConfig]);
  const clientCompiler = multiCompiler.compilers[0];

  app.use(webpackDevMiddleware(multiCompiler, { publicPath, noInfo: true }));
  app.use(webpackHotMiddleware(clientCompiler));
  app.use(
    // keeps serverRender updated with arg: { clientStats, outputPath }
    webpackHotServerMiddleware(multiCompiler, {
      serverRendererOptions: { outputPath }
    })
  );
}
else {
  const clientStats = require('../../dist/buildClient/static/stats.json');
  const serverRender = require('../../dist/buildServer/main.js').default;

  app.use(publicPath, express.static(clientProdConfig.output.path));
  app.use(serverRender({ clientStats, outputPath }));
}

app.listen(3000, () => {
  console.log('Listening @ http://localhost:3000/');
});