import { h, render } from 'preact';
import App from '../shared/App.js';

function init(App) {
	render(
		<App initialState={window.state}/>,
		document.getElementById('root'), document.getElementById('root').lastElementChild
	);
}

const checkFeatures = function() {
	return Promise.all([
		new Promise(resolve => {
			if (!('IntersectionObserver' in window)) {
				console.log("No IntersectionObserver!");
				import('intersection-observer').then(() => resolve());
			} else { resolve(); }
		})
	]).then(() => {
		init(App);
	});
};

// If browser does not support Promises, load polyfill. Then run checkFeatures()
if(!window.Promise) {
	console.log("No promises!");
  var js = document.createElement('script');
  js.src = '/static/js/Promise.js';
  js.onload = function() {
    checkFeatures();
  };
  js.onerror = function() {
    checkFeatures(new Error('Failed to load Promise polyfill.'));
  };
  document.head.appendChild(js);
}

if(window.Promise && ('IntersectionObserver' in window) && Modernizr.objectfit) {
	// All features supported. Go directly to init!
	init(App);
} else {
	// Fetch polyfills before running init()
	checkFeatures();
}



if (process.env.NODE_ENV === 'development') {
	if (module.hot) module.hot.accept('../shared/App.js', () => {
		let App = require('../shared/App.js');
		App = App.default || App;
		init(App);
	});
}